#!/usr/bin/env python

import sys
import os
import subprocess
import pprint

# rootdir  = "triangle-point-samples"
# posdir   = "true"
# negdir   = "false"

rootdir  = ""
posdir   = "true"
negdir   = "false"

logout   = sys.stdout

red      = "\033[0;31m"
green    = "\033[1;32m"
blue     = "\033[1;94m"
nc       = "\033[0m"

exe      = "a.out"

false    = lambda result: result == ""
true     = lambda result: result == "intersects"

def run_tests(subdir, is_correct, is_incorrect):
    currdir = os.getcwd()
    os.chdir(os.path.join(currdir, rootdir, subdir))

    total = 0
    crash = 0
    valid = 0
    invalid = 0
    correct = 0
    incorrect = 0
    
    for sample in os.listdir("."):
        total += 1
        command = os.path.join(currdir, exe)
        bench = subprocess.Popen([command], stdin=open(sample, "r"), stdout=subprocess.PIPE)
        output = bench.communicate()[0].strip()
        exitcode = bench.wait()

        if exitcode != 0:
            crash += 1
            logout.write(blue + sample + nc + "\t" + " - ")
            logout.write(red + "CRASH" + nc)
            if output != "":
                logout.write(output)
            logout.write("\n")
            continue
        
        if is_correct(output):
            valid += 1
            correct += 1
            # logout.write(blue + sample + nc + "\t" + " - ")
            # logout.write(green + "PASS" + nc)
            # logout.write("\n")
        elif is_incorrect(output):
            valid += 1
            incorrect += 1
            logout.write(blue + sample + nc + "\t" + " - ")            
            logout.write(red + "FAILED" + nc)
            logout.write("\n")
        else:
            logout.write(blue + sample + nc + "\t" + " - ")
            logout.write(red + "INVALID" + nc)
            invalid += 1
            logout.write("\n")
            logout.write(output)
            logout.write("\n")

    os.chdir(currdir)
    return {
        "total": total,
        "crash": crash,
        "valid": valid,
        "invalid": invalid,
        "correct": correct,
        "incorrect": incorrect
    }

print "Test negatives..."
true_results = run_tests(negdir, false, true)
print "=== Negative Summary ==="
pprint.pprint(true_results)

print "Test positives..."
false_results = run_tests(posdir, true, false)
print "=== Positive Summary ==="
pprint.pprint(false_results)

print "\t|\t Correct \t|\t Incorrect"
print "Positive \t|\t %d \t|\t %d |" % (true_results["correct"], true_results["incorrect"])
print "Negative \t|\t %d \t|\t %d |" % (false_results["correct"], false_results["incorrect"])
