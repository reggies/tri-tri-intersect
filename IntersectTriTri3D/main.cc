#include <iostream>

#include "tri-gems.h"

int main()
{
  double points[18];

  for(auto& p : points) {
    std::cin >> p;
  }

  if (NatalinA::intersect(&points[0], &points[9])) {
    std::cout << "intersects" << std::endl;
  }
}
