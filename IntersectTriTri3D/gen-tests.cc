#include "gen-tests.hh"
#include "tri-gems.h"

#include <iostream>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <random>
#include <cassert>

const double kEpsilon = 1e-5;

struct vec3 {
  double x, y, z;
};

bool fequal(double alice, double bob)
{
  return std::fabs(alice - bob) < kEpsilon;
}

vec3 operator+(const vec3& alice, const vec3& bob)
{
  return { alice.x + bob.x, alice.y + bob.y, alice.z + bob.z };
}

vec3 operator-(const vec3& alice, const vec3& bob)
{
  return { alice.x - bob.x, alice.y - bob.y, alice.z - bob.z };
}

vec3 operator*(const vec3& v, double t)
{
  return { v.x * t, v.y * t, v.z * t };
}

vec3 operator/(const vec3& v, double t)
{
  return { v.x / t, v.y / t, v.z / t };
}

bool operator!=(const vec3& alice, const vec3& bob)
{
  return !fequal(alice.x, bob.x) || !fequal(alice.y, bob.y) || !fequal(alice.z, bob.z);
}

bool operator==(const vec3& alice, const vec3& bob)
{
  return fequal(alice.x, bob.x) && fequal(alice.y, bob.y) && fequal(alice.z, bob.z);
}

double dot(vec3 alice, vec3 bob)
{
  return alice.x * bob.x + alice.y * bob.y + alice.z * bob.z;
}

vec3 cross(vec3 alice, vec3 bob)
{
  return { alice.y * bob.z - alice.z * bob.y,
        alice.z * bob.x - alice.x * bob.z,
        alice.x * bob.y - alice.y * bob.x };
}

double length(vec3 v)
{
  return std::sqrt(dot(v, v));
}

vec3 normalize(vec3 v)
{
  auto len = length(v);
  if (fabs(len) < kEpsilon) return vec3 { 1, 0, 0 };
  return v / len;
}

vec3 lerp(vec3 alice, vec3 bob, double t)
{
  return alice * t + bob * (1.f - t);
}

bool inbetween(double v, double s0, double s1)
{
  return v > s0 - kEpsilon && v < s1 + kEpsilon;
}

double clamp(double t, double min, double max)
{
  return (t < min ? min : (t > max ? max : t));
}

namespace std {
std::ostream& operator<<(std::ostream& os, const vec3& v)
{
  return os << v.x << ' ' << v.y << ' ' << v.z;
}
}

vec3 generate_point(std::default_random_engine& gen, double mean = 0.f, double stddev = 1.f)
{
  std::normal_distribution<double> dist(mean, stddev);
  return vec3 { dist(gen), dist(gen), dist(gen) };
}

void generate_triangle(std::default_random_engine& gen, vec3 point, vec3 *vs)
{
  {
    std::normal_distribution<double> dist_x(0.f, 1.f);
    std::normal_distribution<double> dist_y(0.f, 1.f);
    std::normal_distribution<double> dist_z(0.f, 1.f);
    vs[0] = point + vec3 {dist_x(gen), dist_y(gen), dist_z(gen)};
    vs[1] = point + vec3 {dist_x(gen), dist_y(gen), dist_z(gen)};
  }

  {
    auto mid = (vs[1] + vs[0]) * .5f;
    auto n = point - mid;
    auto center = point + n * 2;
    std::normal_distribution<double> dist_x(0.f, .00005f);
    std::normal_distribution<double> dist_y(0.f, .0005f);
    std::normal_distribution<double> dist_z(0.f, .0005f);
    auto delta = vec3 {dist_x(gen), dist_y(gen), dist_z(gen)};
    vs[2] = center + delta;
  }
}

void vec3_array_to_double(vec3* vs, size_t count, double* ds)
{
  for(size_t i = 0; i < count; ++i) {
    auto x = i*3;
    auto y = i*3+1;
    auto z = i*3+2;
    ds[x] = vs[i].x;
    ds[y] = vs[i].y;
    ds[z] = vs[i].z;
  }
}

bool intersect(vec3 a[3], vec3 b[3])
{
  double t1[9], t2[9];
  vec3_array_to_double(a, 3, t1);
  vec3_array_to_double(b, 3, t2);
  return NatalinA::intersect(t1, t2);
}

std::string get_sample_path(const char* dir, int index, bool correct)
{
  std::ostringstream oss;
  oss << dir << '/';
  oss << (correct ? "true" : "false") << '/';
  oss << index++ << '.' << "txt";
  return oss.str();
}

void generate_triangle_point_samples(const char* dir)
{
  std::default_random_engine generator;
  std::uniform_int_distribution<int> scale_dist(1, 100);
  
  for(int i = 0; i < 50000; ++i) {
    vec3 point = generate_point(generator);

    vec3 tri1[3];
    generate_triangle(generator, point, tri1);

    vec3 tri2[3];
    std::fill(tri2, tri2 + 3, point);

    auto scale = scale_dist(generator);
    for(auto& v : tri1) v = v * scale;
    for(auto& v : tri2) v = v * scale;

    auto result = intersect(tri1, tri2);
    auto path = get_sample_path(dir, i, result);

    std::ofstream fout(path);
    for(auto v : tri1) fout << v << std::endl;
    fout << std::endl;
    for(auto v : tri2) fout << v << std::endl;
  }
}
