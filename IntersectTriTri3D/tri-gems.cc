#include <iostream>
#include <algorithm>
#include <tuple>

#include <cassert>
#include <cmath>

#include "tri-gems.h"

#define require
// #define require assert

namespace NatalinA {

namespace {

struct vec3 {
  double x, y, z;
};

struct segment {
  vec3 u, v;
};

}

}

namespace std {

std::ostream& operator<<(std::ostream& os, const NatalinA::vec3& v)
{
  return os << '(' << v.x << ',' << v.y << ',' << v.z << ')';
}
std::istream& operator>>(std::istream& is, NatalinA::vec3& v)
{
  return is >> v.x >> v.y >> v.z;
}

}

namespace NatalinA {

namespace {

const double kEpsilon = 1e-5;

bool fequal(double alice, double bob)
{
  return std::fabs(alice - bob) < kEpsilon;
}

vec3 operator+(const vec3& alice, const vec3& bob)
{
  return { alice.x + bob.x, alice.y + bob.y, alice.z + bob.z };
}

vec3 operator-(const vec3& alice, const vec3& bob)
{
  return { alice.x - bob.x, alice.y - bob.y, alice.z - bob.z };
}

vec3 operator*(const vec3& v, double t)
{
  return { v.x * t, v.y * t, v.z * t };
}

vec3 operator/(const vec3& v, double t)
{
  return { v.x / t, v.y / t, v.z / t };
}

bool operator!=(const vec3& alice, const vec3& bob)
{
  return !fequal(alice.x, bob.x) || !fequal(alice.y, bob.y) || !fequal(alice.z, bob.z);
}

bool operator==(const vec3& alice, const vec3& bob)
{
  return fequal(alice.x, bob.x) && fequal(alice.y, bob.y) && fequal(alice.z, bob.z);
}

double dot(vec3 alice, vec3 bob)
{
  return alice.x * bob.x + alice.y * bob.y + alice.z * bob.z;
}

vec3 cross(vec3 alice, vec3 bob)
{
  return { alice.y * bob.z - alice.z * bob.y,
           alice.z * bob.x - alice.x * bob.z,
           alice.x * bob.y - alice.y * bob.x };
}

double length(vec3 v)
{
  return std::sqrt(dot(v, v));
}

vec3 normalize(vec3 v)
{
  auto len = length(v);
  if (fabs(len) < kEpsilon) return vec3 { 1, 0, 0 };
  return v / len;
}

vec3 lerp(vec3 alice, vec3 bob, double t)
{
  return alice * t + bob * (1.f - t);
}

bool inbetween(double v, double s0, double s1)
{
  return v > s0 - kEpsilon && v < s1 + kEpsilon;
}

double clamp(double t, double min, double max)
{
  return (t < min ? min : (t > max ? max : t));
}

// Return real number d which such as
//  d * (s1 - s0) + s0 = closest point to p which belongs to s
double point_segment_closest_point(vec3 p, segment s)
{
  auto dir = s.v - s.u;
  auto lensq = dot(dir, dir);
  if (fequal(lensq, 0.f))
    return 0.f;
  return clamp(dot(p - s.u, dir) / lensq, 0.f, 1.f);
}

bool point_segment_intersect(vec3 p, segment s)
{
  auto delta = point_segment_closest_point(p, s);
  return length(lerp(s.u, s.v, 1.f - delta) - p) <= kEpsilon;
}

// The implementation follows http://paulbourke.net/geometry/pointlineplane/
std::tuple<double, double> line_line_closest_point(segment s0, segment s1)
{
  require(length(s0.u - s0.v) > kEpsilon);
  require(length(s1.u - s1.v) > kEpsilon);
  auto v02 = s0.u - s1.u;
  auto v32 = s1.v - s1.u;
  auto v10 = s0.v - s0.u;
  auto vA = dot(v02, v32);
  auto vB = dot(v32, v10);
  auto vC = dot(v32, v32);
  auto vD = dot(v02, v10);
  auto vE = dot(v10, v10);
  auto det = vE * vC - vB * vB;
  if (fequal(det, 0.f)) {
    return std::make_tuple(0.f, vA / vC);
  }
  auto invDet = 1.0 / det;
  auto d = invDet * (vA * vB - vD * vC);
  return std::make_tuple(d, (vA + d * vB) / vC);
}

vec3 segment_closest_endpoint(segment s, double t)
{
  if (t < .5f)
    return s.u;
  else
    return s.v;
}

std::tuple<double, double> segment_segment_closest_point(segment s0, segment s1)
{
  double d0, d1;
  std::tie(d0, d1) = line_line_closest_point(s0, s1);

  if (!inbetween(d0, 0.f, 1.f) && !inbetween(d1, 0.f, 1.f)) {
    // The closest point doesn't belong to any of the segments

    // Choose whichever segment endpoint is the closest to the closest point
    auto p0 = segment_closest_endpoint(s0, d0);
    auto p1 = segment_closest_endpoint(s1, d1);

    auto t0 = point_segment_closest_point(p1, s0);
    auto closest_point0 = lerp(s0.u, s0.v, 1.f - t0);

    auto t1 = point_segment_closest_point(p0, s1);
    auto closest_point1 = lerp(s1.u, s1.v, 1.f - t1);

    if (length(closest_point0 - p1) <= length(closest_point1 - p0)) {
      d0 = t0;
      d1 = clamp(d1, 0.f, 1.f);
    } else {
      d0 = clamp(d0, 0.f, 1.f);
      d1 = t1;
    }
  } else if(inbetween(d0, 0.f, 1.f)) {
    // S0 closest point is found; find S1 closest point
    d0 = point_segment_closest_point(segment_closest_endpoint(s1, d1), s0);
    d1 = clamp(d1, 0.f, 1.f);
  } else if (inbetween(d1, 0.f, 1.f)) {
    // S1 closest point is found; find S0 closest point
    d1 = point_segment_closest_point(segment_closest_endpoint(s0, d0), s1);
    d0 = clamp(d0, 0.f, 1.f);
  }

  return std::make_pair(d0, d1);
}

bool segment_segment_intersect(segment s0, segment s1)
{
  require(length(s0.u - s0.u) > kEpsilon);
  require(length(s1.u - s1.u) > kEpsilon);

  double d0, d1;
  std::tie(d0, d1) = segment_segment_closest_point(s0, s1);

  auto p0 = lerp(s0.u, s0.v, 1.f - d0);
  auto p1 = lerp(s1.u, s1.v, 1.f - d1);

  return length(p1 - p0) <= kEpsilon;
}

bool triangle_point_intersect_barycentric(double v0x, double v0y, double v1x, double v1y, double v2x, double v2y)
{
  auto det = v1y * v0x - v1x * v0y;
  if (fequal(det, 0.f))
    return false;

  auto u = (v1y * v2x - v1x * v2y) / det;
  auto v = (v2y * v0x - v0y * v2x) / det;

  return inbetween(u, 0.f, 1.f) && inbetween(v, 0.f, 1.f) &&
      inbetween(u + v, 0.f, 1.f);
}

// http://en.wikipedia.org/wiki/Barycentric_coordinates_%28mathematics%29
bool triangle_point_intersect(vec3 tri[3], vec3 p)
{
  // TODO why so rough?
  const auto dist_threshold = 1e-3f;
  auto v0 = tri[0] - tri[2];
  auto v1 = tri[1] - tri[2];
  auto v2 = p - tri[2];
  auto norm = normalize(cross(v0, v1));

  if (std::fabs(dot(v2, norm)) > dist_threshold) {
    return false;
  }

  // Project point onto axis-aligned plane
  auto x = &vec3::x;
  auto y = &vec3::y;
  if (std::fabs(norm.x) >= std::max(std::fabs(norm.z), std::fabs(norm.y))) {
    x = &vec3::y;
    y = &vec3::z;
  } else if (std::fabs(norm.y) >= std::fabs(norm.z)) {
    x = &vec3::x;
    y = &vec3::z;
  }

  return triangle_point_intersect_barycentric(v0.*x, v0.*y, v1.*x, v1.*y, v2.*x, v2.*y);
}

bool coplanar_triangle_segment_intersect(vec3 tri[3], segment s)
{
  auto norm = cross(tri[1] - tri[0], tri[2] - tri[0]);

  // So far we know that both endpoints of the segment are at the same distance
  // from the triangle's plane. Check the distance for one of them and
  // see if we can eliminate any further computations.
  if (!fequal(dot(s.u, norm), dot(norm, tri[0]))) {
    return false;
  }

  // Consider the case of a segment and a triangle when the triangle includes both
  // ends of the segment
  if (triangle_point_intersect(tri, s.u) || triangle_point_intersect(tri, s.v)) {
    return true;
  }

  for(int i = 0; i < 3; ++i) {
    if (segment_segment_intersect(segment { tri[i], tri[(i+1)%3] }, s)) {
      return true;
    }
  }

  return false;
}

// Test based on Moller05 method Minimum Storage Ray/Triangle Intersection. 2005
// http://www.cs.virginia.edu/~gfx/Courses/2003/ImageSynthesis/papers/Acceleration/Fast%20MinimumStorage%20RayTriangle%20Intersection.pdf
bool triangle_segment_intersect(vec3 tri[3], segment s)
{
  auto dir = s.v - s.u;

  // Edge vectors
  auto e1 = tri[1] - tri[0];
  auto e2 = tri[2] - tri[0];

  // Solve Q + t*D = b1*e1 + b2*e2
  // Compute determinant
  auto vP = cross(dir, e2);
  auto det = dot(e1, vP);
  // If determinant is zero, the segment lies in the plane of triangle
  if (fequal(det, 0.f)) {
    return coplanar_triangle_segment_intersect(tri, s);
  }

  // Calculate distance from t0 to ray origin
  auto vT = s.u - tri[0];

  // Barycentric u
  auto u = dot(vT, vP) / det;
  if (u < -kEpsilon || u > 1.f + kEpsilon)
    return false;

  auto vQ = cross(vT, e1);

  // Barycentric v
  auto v = dot(dir, vQ) / det;
  if (v < -kEpsilon || u + v > 1.f + kEpsilon)
    return false;

  return inbetween(dot(e2, vQ) / det, 0.f, 1.f);
}

bool interval_interval_intersect(double u0, double u1, double v0, double v1)
{
  return u1 >= v0 && u0 <= v1;
}

bool triangle_triangle_intersect(vec3 alice[3], vec3 bob[3])
{
  require(alice[0] != alice[1]);
  require(alice[1] != alice[2]);
  require(alice[2] != alice[0]);

  require(bob[0] != bob[1]);
  require(bob[1] != bob[2]);
  require(bob[2] != bob[0]);

  for(int i = 0; i < 3; ++i) {
    if (triangle_segment_intersect(alice, segment { bob[i], bob[(i+1)%3] }))
      return true;
  }
  return false;
}

bool is_point(vec3 t[3])
{
  return t[0] == t[1] && t[0] == t[2];
}

bool is_segment(vec3 t[3], segment& s)
{
  auto a = t[1] - t[0];
  auto b = t[2] - t[0];

  if (length(cross(a, b)) <= kEpsilon) {
    // Calculate segment endpoints
    auto t0t1 = length(t[0] - t[1]);
    auto t1t2 = length(t[1] - t[2]);
    auto t0t2 = length(t[0] - t[2]);

    if (t0t1 >= std::max(t1t2, t0t2)) {
      s = segment { t[0], t[1] };
    } else if (t1t2 >= std::max(t0t1, t0t2)) {
      s = segment { t[1], t[2] };
    } else {
      s = segment { t[0], t[2] };
    }

    return true;
  }
  return false;
}

bool point_point_intersect(vec3 alice, vec3 bob)
{
  return alice == bob;
}

bool generic_intersect(vec3 alice[3], vec3 bob[3])
{
  // Segments end points
  segment alice_as_segment, bob_as_segment;

  if (is_point(alice)) {
    if (is_point(bob)) {
      return point_point_intersect(alice[0], bob[0]);
    }
    else if(is_segment(bob, bob_as_segment)) {
      return point_segment_intersect(alice[0], bob_as_segment);
    }
    else {
      return triangle_point_intersect(bob, alice[0]);
    }
  } else if(is_segment(alice, alice_as_segment)) {
    if (is_point(bob)) {
      return point_segment_intersect(bob[0], alice_as_segment);
    }
    else if(is_segment(bob, bob_as_segment)) {
      return segment_segment_intersect(alice_as_segment, bob_as_segment);
    }
    else {
      return triangle_segment_intersect(bob, alice_as_segment);
    }
  } else {
    if (is_point(bob)) {
      return triangle_point_intersect(alice, bob[0]);
    }
    else if(is_segment(bob, bob_as_segment)) {
      return triangle_segment_intersect(alice, bob_as_segment);
    }
    else {
      return triangle_triangle_intersect(alice, bob)
          || triangle_triangle_intersect(bob, alice);
    }
  }
}

void vec3_array_from_points(double* points, size_t count, vec3* vs)
{
  for(size_t i = 0; i < count; ++i) {
    auto x = i*3;
    auto y = i*3+1;
    auto z = i*3+2;
    vs[i] = { points[x], points[y], points[z] };
  }
}

} // anonymous namespace

bool intersect(double tri1[9], double tri2[9])
{
  vec3 v1[3], v2[3];

  vec3_array_from_points(tri1, 3, v1);
  vec3_array_from_points(tri2, 3, v2);

  return generic_intersect(v1, v2);
}

} // namespace NatalinA
