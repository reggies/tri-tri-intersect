#ifndef __TRI_GEMS_H__
#define __TRI_GEMS_H__

namespace NatalinA {

bool intersect(double tri1[9], double tri2[9]);

}

#endif  // __TRI_GEMS_H__
